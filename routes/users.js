var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var LastFmStrategy = require('passport-lastfm');
var _ = require('lodash');
var request = require('request');

var User = require('../models/users');

// Register
router.get('/register', function(req, res){
    res.render('register');
});

// Login
router.get('/login', function(req, res){
    res.render('login');
});

// Register User
router.post('/register', function(req, res){
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var name = first_name + ' ' + last_name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var password2 = req.body.password2;

    // Validation
    req.checkBody('first_name', 'First name is required').notEmpty();
    req.checkBody('last_name', 'Last name is required').notEmpty();
    req.checkBody('email', 'Email is required').isEmail();
    req.checkBody('username', 'Username is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password);
    
    var errors = req.validationErrors();

    if(errors){
    	res.render('register', {
    		errors:errors
    	});
    } else {
    //checking for email and username are already taken
    User.findOne({ username: { 
        "$regex": "^" + username + "\\b", "$options": "i"
     }}, function (err, user) {    
        User.findOne({ email: { 
            "$regex": "^" + email + "\\b", "$options": "i"
            }}, function (err, mail) {
                if (user || mail) {
                    if(user){
                        req.flash('errors_msg', 'Username Taken');
                    } else {
                        req.flash('error_msg', 'You are logged out');
                    }
                    res.render('register', {
                        user: user,
                        mail: mail
                    });
                }
                else{
                	var newUser = new User({
                        first_name: first_name,
                        last_name: last_name,
                        email: email,
                        username: username,
                        password: password
                    });

                    User.createUser(newUser, function(err, user){
                        if(err) throw err;
                        console.log(user);
                    });

                    req.flash('success_msg', 'You are registered and can now login');

                    res.redirect('/users/login');
                }
            });
        });
    }
});

/*var cb_url = 'http://localhost:8080';
passport.use(new LastFMStrategy({
  'api_key': '045dea0ca539f6fdaa42803b1e3c3b41',
  'secret': '7ecdccb96b28ef208ad499d4b24d6fb7',
  'callbackURL': cb_url + '/auth/lastfm/callback'
}, function(token, tokenSecret, profile, done) {
    console.log(profile);
    console.log("NOTHING");
    var searchQuery = {
      name: profile.displayName
    };

    var updates = {
      name: profile.displayName,
      someID: profile.id
    };

    var options = {
      upsert: true
    };

    // update the user if s/he exists or add a new user
    User.findOneAndUpdate(searchQuery, updates, options, function(err, user) {
      if(err) {
        return done(err);
      } else {
        return done(null, user);
      }
    });
  }
));*/



/*var request = require('request');

request.post(
    'http://www.yoursite.com/formpage',
    { json: { key: 'value' } },
    function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body)
        }
    }
);*/


var cb_url = 'http://localhost:8000';
var api_key = '3c4ef13e357295b367e750270ed9cfd1';
var api_secret = 'f5c197288dc5ff2788d1d56c5446ff7d';
var userName = '';
passport.use(new LastFmStrategy({
  'api_key': api_key,
  'secret': api_secret,
  'callback_url': cb_url + '/users/auth/lastfm/callback'
}, function(req, sessionKey, done) {
  // Find/Update user's lastfm session
  if(sessionKey){
    userName = sessionKey.name;
}

  
  var url_base = 'http://ws.audioscrobbler.com/2.0/?method=';
  var queries = ['user.getrecenttracks', 'user.gettopalbums', 'user.gettoptracks', 'user.gettopartists'];
  user_data = [];

  var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
  var xhr = new XMLHttpRequest();

  var q;
  for(q = 0; q < queries.length; q++){
    var url = url_base+queries[q]+'&user='+userName+'&api_key='+api_key+'&format=json';

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.send();
    data = JSON.parse(xhr.responseText);
    key = Object.keys(data)[0];
    inner_key = Object.keys(data[key])[0];
    inner_data = data[key][inner_key];
    user_data.push({key: key, value: inner_data});
  }
  
  // If user logged in
  if (req.user){
    User.findById(req.user.id, (err, user) => {
      if (err) return done(err);

      var creds = _.find(req.user.tokens, {type:'lastfm'});

      user_tokens = req.user.tokens
      var i;
      for (i = 0; i < user_tokens.length; i++) {
          if(user_tokens[i].type === "lastfm"){
            req.flash('success_msg', 'Account already linked');
            return done(err, user, {user_data: user_data});
          }
        } 


      // if creds already present
      if (user.lastfm && creds){
        req.flash('success_msg', 'Account already linked');
        //return done(err, user, {msg:'Account already linked'})
        return done(err, user);
      }

      else{
        user.tokens.push({type:'lastfm', username:sessionKey.username, key:sessionKey.key });
        user.lastfm = sessionKey.key;

        user.save(function(err){
          if (err) return done(err);
          req.flash('success_msg', "Last.fm authentication success");

          return done(err, user, sessionKey);
        });
      }
    });
  }
  else{
  
    return done(null, false, {error_msg:'Must be logged in'});
  }
}));

passport.use(new LocalStrategy(
    function(username, password, done){
        /*User.findOne({username: username}, function(err, user){
            if(err) { return done(err); }
            if(!user) {
                return done(null, false, {message: 'Incorrest username.'});
            }
            if(!user.validPassword(password)) {
                return done(null, false, {message: 'Incorrect password.'});
            }
            return done(null, user);
        });*/

        User.getUserByUsername(username, function(err, user){
            if(err) throw err;
            if(!user){
                return done(null, false, {message: 'User not found'});
            }
            User.comparePassword(password, user.password, function(err, isMatch){
                if(err) throw err;
                if(isMatch){
                    return done(null, user);
                } else {
                    return done(null, false, {message: 'Invalid Password'});
                }
            });
        });
    }
    ));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

router.post('/login',
    passport.authenticate('local', {successRedirect:'/', failureRedirect:'/users/login', failureFlash: true}),
        function(req, res){
            res.redirect('/');
//            res.render('index');
        });

router.get('/logout', function(req, res){
    req.logout();

    req.flash('success_msg', 'You are logged out');

    res.redirect('/users/login');
});

router.get('/auth/lastfm', passport.authenticate('lastfm'));
router.get('/auth/lastfm/callback', function(req, res, next){
  passport.authenticate('lastfm', {failureRedirect:'/'}, function(err, user, sesh){
    res.redirect('/');
  })(req, {} );
});


module.exports = router
