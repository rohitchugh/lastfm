# lastfm music app


This is a simple music app using Node.js, Express, Passport, Mongoose and Last.fm api

### Version
1.1.0

### Usage


### Installation
App requires [Node.js](https://nodejs.org/) v4+ to run

Clone Repository to your local machine
Navigate to repository using terminal


```sh
$ npm install package.json
```

```sh
$ node app.js
```

Open browser and visit
### localhost:8000